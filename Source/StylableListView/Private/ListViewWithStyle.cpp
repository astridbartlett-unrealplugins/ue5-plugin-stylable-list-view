#include "ListViewWithStyle.h"

UListViewWithStyle::UListViewWithStyle(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	  Style(FAppStyle::Get().GetWidgetStyle<FTableViewStyle>("ListView"))
{
}

TSharedRef<STableViewBase> UListViewWithStyle::RebuildListWidget()
{
	TSharedRef<SListView<UObject*>> NewWidget = ConstructListView<SListView>();

	NewWidget->SetStyle(&Style);

	return NewWidget;
}
