#pragma once

#include "CoreMinimal.h"
#include "Components/ListView.h"
#include "ListViewWithStyle.generated.h"

UCLASS(Blueprintable)
class STYLABLELISTVIEW_API UListViewWithStyle : public UListView
{
	GENERATED_BODY()
	
public:
	UListViewWithStyle(const FObjectInitializer& ObjectInitializer);
	
protected:
	virtual TSharedRef<STableViewBase> RebuildListWidget() override;

protected:
	UPROPERTY(EditAnywhere, Category = "Style")
	FTableViewStyle Style;
};
